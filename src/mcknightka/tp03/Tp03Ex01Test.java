package mcknightka.tp03;

import static org.junit.Assert.*;
import org.junit.Test;//pour tester avec vscode

// import static org.junit.jupiter.api.Assertions.*;
// import org.junit.jupiter.api.Test;

class Tp03Ex01Test {
	StringBuilder[] voiesRef, voies;
	StringBuilder[] trainsRef, trains;
	
	Tp03Ex01Test() {
		voiesRef = new StringBuilder[Tp03Ex01.NB_VOIES];
		for (int i = 0; i < voiesRef.length; ++i) { voiesRef[i] = new StringBuilder(); }
		voies = new StringBuilder[Tp03Ex01.NB_VOIES];
		for (int i = 0; i < voies.length; ++i) { voies[i] = new StringBuilder(); }
		trainsRef = new StringBuilder[Tp03Ex01.NB_TRAINS];
		for (int i = 0; i < trainsRef.length; ++i) { trainsRef[i] = new StringBuilder(); }
		trains = new StringBuilder[Tp03Ex01.NB_TRAINS];
		for (int i = 0; i < trains.length; ++i) { trains[i] = new StringBuilder(); }
	}

	/**
	 * Permet de comparer les deux cours de triage (normale et référence)
	 */
	void assertCour() {
		for (int i = 0; i < Tp03Ex01.NB_VOIES; ++i) {
			assertEquals(voiesRef[i].toString(), voies[i].toString());
		}
		for (int i = 0; i < Tp03Ex01.NB_TRAINS; ++i) {
			assertEquals(trainsRef[i].toString(), trains[i].toString());
		}		
	}
	
	@Test
	void testCmdNouveau() {
		// TEST DÉMO #1
		Tp03Ex01.cmdInit(voiesRef, trainsRef, "i:ACEAC-FFNNN-CAFECA-ACEFIV-PPACEVVV:PAPE-NFFAA");
		Tp03Ex01.cmdInit(voies, trains, "i:ACEAC-FFNNN-CAFECA-ACEFIV-PPACEVVV:-NFFAA");
		assertEquals("", Tp03Ex01.cmdNouveau(trains, "n:1,PAPE"), 
				"Remplacer un train vide");
		assertCour();

		// TEST DÉMO #2
		Tp03Ex01.cmdInit(voiesRef, trainsRef, "i:ACEAC-FFNNN-CAFECA-ACEFIV-PPACEVVV:CCPPP-NFFAA");
		Tp03Ex01.cmdInit(voies, trains, "i:ACEAC-FFNNN-CAFECA-ACEFIV-PPACEVVV:CCPPP-NFFAA");
		assertEquals("Numéro de train invalide", Tp03Ex01.cmdNouveau(trains, "n:5,CAFE"), 
				"Numéro de train > nombre trains");
		assertCour();
		
		// TODO: FAIRE 5 TESTS
	}

	@Test
	void testCmdAttache() {
		// TODO: FAIRE 5 TESTS
	}

	@Test
	void testCmdDetache() {
		// TODO: FAIRE 5 TESTS
	}

	@Test
	void testCmdDeplace() {
		// TODO: FAIRE 5 TESTS
	}
}
