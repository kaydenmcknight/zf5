package mcknightka.at05;

import java.util.*;

import commun.Outils;

public class At05Ex01Main {
	public static final int DIM_TEST = 100000; // Dimension du vecteur de test
	public static final boolean TEST1_EXE = false; // Active le test #1
	public static final boolean TEST2_EXE = false; // Active le test #2
	public static final boolean TEST3_EXE = true; // Active le test #3
	public static final boolean TEST4_EXE = false; // Active le test #4
	public static final boolean TEST5_EXE = false; // Active le test #5

	// 1/PROB_INVALID: Probabilité que la valeur à trouver ne soit pas dans le tableau
	public static final int PROB_INVALID = 50; // 2% de valeurs introuvables
	
	public static void main(String[] args) {
		Outils.Aleatoire.setSeed(12345);
		
		long delai;
		// Vecteur de base qui sera copié et utilisé pour tous les tests
		long[] vecOriginal = genereVecAleatoire(DIM_TEST);

		if (TEST1_EXE) { // Tri ou fusion en premier?
			System.out.print(String.format("%-15s", "Test 1: "));
			delai = test1A(vecOriginal);
			System.out.print(String.format("%,20d : ", delai).replace('\u00A0', ' '));
			delai = test1B(vecOriginal);
			System.out.println(String.format("%,20d", delai).replace('\u00A0', ' '));
		}
		
		if (TEST2_EXE) { // Ajout trié ou non trié? pour différents nombres d'ajouts
			for (int i = 1; i <= 10; ++i) {
				System.out.print(String.format("%-15s", "Test 2 (" + i * 10 + "%): "));
				delai = test2A(vecOriginal, i * 10);
				System.out.print(String.format("%,20d : ", delai).replace('\u00A0', ' '));
				delai = test2B(vecOriginal, i * 10);
				System.out.println(String.format("%,20d", delai).replace('\u00A0', ' '));
			}
		}

		if (TEST3_EXE) { // Recherche séquentielle vs binaire
			System.out.print(String.format("%-15s", "Test 3: "));
			delai = test3A(vecOriginal);
			System.out.print(String.format("%,20d : ", delai).replace('\u00A0', ' '));
			delai = test3B(vecOriginal);
			System.out.println(String.format("%,20d", delai).replace('\u00A0', ' '));
		}
		
		if (TEST4_EXE) { // Retrait trié ou non trié? pour différents nombres de retrait
			for (int i = 1; i <= 10; ++i) {
				System.out.print(String.format("%-15s", "Test 4 (" + i * 10 + "%): "));
				delai = test4A(vecOriginal, i * 10);
				System.out.print(String.format("%,20d : ", delai).replace('\u00A0', ' '));
				delai = test4B(vecOriginal, i * 10);
				System.out.println(String.format("%,20d", delai).replace('\u00A0', ' '));
			}
		}
		
		if (TEST5_EXE) { // Retrait trié avec recherche séquentielle ou binaire
			System.out.print(String.format("%-15s", "Test 5: "));
			delai = test5A(vecOriginal, 75);
			System.out.print(String.format("%,20d : ", delai).replace('\u00A0', ' '));
			delai = test5B(vecOriginal, 75);
			System.out.println(String.format("%,20d", delai).replace('\u00A0', ' '));
		}
	}

	/**
	 * Fusion de 2 vecteurs de taille égale non triés suivi d'un tri
	 * @param vecOriginal Vecteur aléatoire pour les tests
	 */
	public static long test1A(long[] vecOriginal) {
		long[] vecGa, vecDr, vecFu;
		int vecGaNb, vecDrNb, vecFuNb;
		long debut, fin;

		// Initialisation des vecteurs
		vecGa = Arrays.copyOfRange(vecOriginal, 0, vecOriginal.length/2);
		vecGaNb = vecGa.length;
		vecDr = Arrays.copyOfRange(vecOriginal, vecOriginal.length/2, vecOriginal.length);
		vecDrNb = vecDr.length;
		vecFu = new long[vecGaNb + vecDrNb];

		// Exécution du test et calcul du temps requis
		debut = System.nanoTime();
		vecFuNb = At05Ex01.fusionVec(vecGa, vecGaNb, vecDr, vecDrNb, vecFu, false);
		At05Ex01.triSel(vecFu, vecFuNb);
		fin = System.nanoTime();
		return fin - debut;
	}

	/**
	 * Tri de 2 vecteurs de taille égale suivi de leur fusion
	 * @param vecOriginal Vecteur aléatoire pour les tests
	 */
	public static long test1B(long[] vecOriginal) {
		long[] vecGa, vecDr, vecFu;
		int vecGaNb, vecDrNb, vecFuNb;
		long debut, fin;

		// Initialisation des vecteurs
		vecGa = Arrays.copyOfRange(vecOriginal, 0, vecOriginal.length/2);
		vecGaNb = vecGa.length;
		vecDr = Arrays.copyOfRange(vecOriginal, vecOriginal.length/2, vecOriginal.length);
		vecDrNb = vecDr.length;
		vecFu = new long[vecGaNb + vecDrNb];
		
		// Exécution du test et calcul du temps requis
		debut = System.nanoTime();
		At05Ex01.triSel(vecGa, vecGaNb);
		At05Ex01.triSel(vecDr, vecDrNb);
		vecFuNb = At05Ex01.fusionVec(vecGa, vecGaNb, vecDr, vecDrNb, vecFu, true);
		fin = System.nanoTime();
		return fin - debut;
	}

	/**
	 * Ajout dans un vecteur trié
	 * @param vecOriginal Vecteur aléatoire pour les tests
	 * @param pctLongueur Pourcentage de la longueur à remplir
	 */
	public static long test2A(long[] vecOriginal, int pctLongueur) {
		long[] vec;
		int vecNb;
		long debut, fin;

		// Initialisation du vecteur
		vec = new long[vecOriginal.length];
		vecNb = 0;
		
		// Exécution du test et calcul du temps requis
		debut = System.nanoTime();
		for (int i = 0; i < vec.length*pctLongueur/100; ++i) {
			vecNb = At05Ex01.ajout(vec, vecNb, elemAjout(vecOriginal, i), true);
		}
		fin = System.nanoTime();
		return fin - debut;
	}

	/**
	 * Ajout dans un vecteur non trié suivi d'un tri
	 * @param vecOriginal Vecteur aléatoire pour les tests
	 * @param pctLongueur Pourcentage de la longueur à remplir
	 */
	public static long test2B(long[] vecOriginal, int pctLongueur) {
		long[] vec;
		int vecNb;
		long debut, fin;

		// Initialisation du vecteur
		vec = new long[vecOriginal.length];
		vecNb = 0;
		
		// Exécution du test et calcul du temps requis
		debut = System.nanoTime();
		for (int i = 0; i < vec.length*pctLongueur/100; ++i) {
			vecNb = At05Ex01.ajout(vec, vecNb, elemAjout(vecOriginal, i), false);
		}
		At05Ex01.triSel(vec, vecNb);
		fin = System.nanoTime();
		return fin - debut;
	}

	/**
	 * Recherche séquentielle dans un vecteur trié
	 * @param vecOriginal Vecteur aléatoire pour les tests
	 */
	public static long test3A(long[] vecOriginal) {
		long[] vec;
		int vecNb;
		long debut, fin;

		// Initialisation du vecteur
		vec = vecOriginal.clone();
		vecNb = vec.length;
		Arrays.sort(vec); // Tri (librairie Java) non évalué pour ce test

		// Exécution du test et calcul du temps requis
		debut = System.nanoTime();
		for (int i = 0; i < vecNb; ++i) {
			At05Ex01.rechercheSeq(vec, vecNb, elemRecherche(vecOriginal, i), true);
		}
		fin = System.nanoTime();
		return fin - debut;
	}

	/**
	 * Recherche binaire dans un vecteur trié
	 * @param vecOriginal Vecteur aléatoire pour les tests
	 */
	public static long test3B(long[] vecOriginal) {
		long[] vec;
		int vecNb;
		long debut, fin;

		// Initialisation du vecteur
		vec = vecOriginal.clone();
		vecNb = vec.length;
		Arrays.sort(vec); // Tri (librairie Java) non évalué pour ce test
		
		// Exécution du test et calcul du temps requis
		debut = System.nanoTime();
		for (int i = 0; i < vecNb; ++i) {
			At05Ex01.rechercheBin(vec, vecNb, elemRecherche(vecOriginal, i));
		}
		fin = System.nanoTime();
		return fin - debut;
	}

	/**
	 * Retraits dans un vecteur non trié suivi d'un tri
	 * @param vecOriginal Vecteur aléatoire pour les tests
	 * @param pctLongueur Pourcentage des valeurs à retirer avant le tri
	 */
	public static long test4A(long[] vecOriginal, int pctLongueur) {
		long[] vec;
		int vecNb;
		long debut, fin;

		// Initialisation du vecteur
		vec = vecOriginal.clone();
		vecNb = vec.length;
		Arrays.sort(vec); // Tri initial (librairie Java) non évalué pour ce test

		// Exécution du test et calcul du temps requis
		debut = System.nanoTime();
		for (int i = 0; i < vec.length*pctLongueur/100; ++i) {
			vecNb = At05Ex01.retrait(vec, vecNb, elemRecherche(vecOriginal, i), 'N');
		}
		At05Ex01.triSel(vec, vecNb);
		fin = System.nanoTime();
		return fin - debut;
	}

	/**
	 * Retraits dans un vecteur trié
	 * @param vecOriginal Vecteur aléatoire pour les tests
	 * @param pctLongueur Pourcentage des valeurs à retirer
	 */
	public static long test4B(long[] vecOriginal, int pctLongueur) {
		long[] vec;
		int vecNb;
		long debut, fin;

		// Initialisation du vecteur
		vec = vecOriginal.clone();
		vecNb = vec.length;
		Arrays.sort(vec); // Tri initial (librairie Java) non évalué pour ce test
		
		// Exécution du test et calcul du temps requis
		debut = System.nanoTime();
		for (int i = 0; i < vec.length*pctLongueur/100; ++i) {
			vecNb = At05Ex01.retrait(vec, vecNb, elemRecherche(vecOriginal, i), 'S');
		}
		fin = System.nanoTime();
		return fin - debut;
	}

	/**
	 * Retraits dans un vecteur trié en utilisant la recherche binaire
	 * @param vecOriginal Vecteur aléatoire pour les tests
	 * @param pctLongueur Pourcentage des valeurs à retirer
	 */
	public static long test5A(long[] vecOriginal, int pctLongueur) {
		long[] vec;
		int vecNb;
		long debut, fin;

		// Initialisation du vecteur
		vec = vecOriginal.clone();
		vecNb = vec.length;
		Arrays.sort(vec); // Tri initial (librairie Java) non évalué pour ce test
		
		// Execution du test et calcul du temps requis
		debut = System.nanoTime();
		for (int i = 0; i < vec.length*pctLongueur/100; ++i) {
			vecNb = At05Ex01.retrait(vec, vecNb, elemRecherche(vecOriginal, i), 'B');
		}
		fin = System.nanoTime();
		return fin - debut;
	}

	/**
	 * Retraits dans un vecteur trié en utilisant la recherche séquentielle
	 * @param vecOriginal Vecteur aléatoire pour les tests
	 * @param pctLongueur Pourcentage des valeurs à retirer
	 */
	public static long test5B(long[] vecOriginal, int pctLongueur) {
		long[] vec;
		int vecNb;
		long debut, fin;

		// Initialisation du vecteur
		vec = vecOriginal.clone();
		vecNb = vec.length;
		Arrays.sort(vec); // Tri initial (librairie Java) non évalué pour ce test

		// Exécution du test et calcul du temps requis
		debut = System.nanoTime();
		for (int i = 0; i < vec.length*pctLongueur/100; ++i) {
			vecNb = At05Ex01.retrait(vec, vecNb, elemRecherche(vecOriginal, i), 'S');
		}
		fin = System.nanoTime();
		return fin - debut;
	}

	/**
	 * Génère un vecteur de nombre aléatoires en long
	 * NOTE: Utilisez cette fonction une seule fois et faites, par la suite,
	 * 		 des copies du vecteur obtenu afin d'uniformiser les tests
	 * @param taille Nombre de valeurs aléatoires voulues dans le vecteur
	 * @return Vecteur avec le nombre voulu de valeurs aléatoires
	 */
	public static long[] genereVecAleatoire(int taille) {
		long[] vecA = new long[taille];
		for (int i = 0; i < taille; ++i) {
			vecA[i] = (long) (Outils.Aleatoire.nextDouble()*Long.MAX_VALUE);
		}
		return vecA;
	}
	
	/**
	 * Sélectionne un élément dans le vecteur de nombres aléatoires
	 * selon la valeur du compteur
	 * @param vecOri Vecteur de nombres aléatoires
	 * @param compteur Valeur utilisée pour choisir le nombre
	 * @return Valeur extraite du vecteur aléatoire
	 */
	public static long elemAjout(long[] vecOri, int compteur) {
		int pos = (int) ((99881L*compteur)%DIM_TEST);
		return vecOri[pos];
	}
	
	/**
	 * Génère un élément à rechercher à partir du vecteur de nombres aléatoires
	 * Un nombre ne se trouvant pas dans le vecteur aléatoire peut être produit
	 * selon une probabilité prédéfinie par PROB_INVALID
	 * @param vecOri Vecteur de nombres aléatoires
	 * @param compteur Valeur utilisée pour choisir le nombre
	 * @return Valeur extraite du vecteur aléatoire ou valeur introuvable
	 */
	public static long elemRecherche(long[] vecOri, int compteur) {
		int pos = (int) ((99881L*compteur)%DIM_TEST);
		long valeur = vecOri[pos];
		if (pos%PROB_INVALID==0) {
			valeur++;
		}
		return valeur;
	}
}
