package mcknightka.at05;

public class At05Ex01 {
	/**
	 * Recherche séquentielle de la première occurence d'une valeur
	 * 
	 * @param vec Tableau contenant les valeurs
	 * @param nbElem Nombre d'éléments contenus dans le tableau
	 * @param valeur Valeur recherchée
	 * @param ordonne Indique si le tableau est ordonné ou non
	 * @return Position de la valeur recherchée, -1 si non trouvée
	 */
	public static int rechercheSeq(long[] vec, int nbElem, long valeur, boolean ordonne) {
		int pos = 0;
		if (ordonne) { // Pour un tableau ordonné
			while (pos < nbElem && vec[pos] < valeur) {
				++pos; 
			}
			if (pos == nbElem || vec[pos] != valeur) {
				pos = -1;				
			}
		} else { // Pour un tableau non ordonné
			while (pos < nbElem && vec[pos] != valeur) {
				++pos;
			}
			if (pos == nbElem) {
				pos = -1;
			}
		}
		return pos;
	}

	/**
	 * Recherche binaire de l'occurence d'une valeur.
	 * NOTE: Le tableau doit être ordonné
	 * 
	 * @param vec Tableau ordonné contenant les valeurs
	 * @param nbElem Nombre d'éléments contenus dans le tableau
	 * @param valeur Valeur recherchée
	 * @return Position de la valeur recherchée, -1 si non trouvée
	 */
	public static int rechercheBin(long[] vec, int nbElem, long valeur) {
		int posGa = 0;
		int posDr = nbElem - 1;
		int posMi = nbElem / 2;

		while (posGa <= posDr && vec[posMi] != valeur) {
			if (vec[posMi] < valeur) {
				posDr = posMi - 1;
			}else{
				posGa = posMi + 1;
			}

			System.out.println(posMi);

			posMi = (posGa + posDr)/2;
		}

		if (!(posGa <= posDr)) { 
			posMi = -1;
		}

		
		// Vous pouvez utiliser l'affichage ci-dessous pour vous aider à deboguer
		//System.out.println("Ga : " + posGa + "     Mi : " + posMi + "     Dr : " + posDr);
		return posMi;
	}
	
	/**
	 * Fonction qui permet de trier le tableau reçu en paramètre
	 * NOTE: L'ordre original du tableau sera perdu
	 * 
	 * @param vec Tableau à trier
	 * @param nbElem Nombre d'éléments contenus dans le tableau
	 */
	public static void triSel(long[] vec, int nbElem) {
		// TODO: Implanter le tri par sélection
		// TODO: Tests unitaires
	}
	
	/**
	 * Permet d'ajouter un élément dans un tableau
	 * 
	 * @param vec Tableau dans lequel l'élément sera ajouté 
	 * @param nbElem Nombre d'éléments contenus dans le tableau
	 * @param elem Valeur à ajouter
	 * @param ordonne Indique si on fait l'ajout dans un tableau ordonné ou non
	 * @return Nouveau nombre d'éléments contenus dans le tableau
	 */
	public static int ajout(long[] vec, int nbElem, long elem, boolean ordonne) {
		// TODO: Verifier s'il reste de la place dans le vecteur
		// TODO: Traiter les vecteurs ordonnés et non ordonnés
		// TODO: Tests unitaires
		return nbElem;
	}

	/**
	 * Retrait d'une valeur dans un tableau
	 * 
	 * @param vec Tableau dans lequel l'élément sera retiré
	 * @param nbElem Nombre d'éléments contenus dans le tableau
	 * @param elem Valeur à retirer
	 * @param typeRech Méthode à utiliser pour trouver l'élément à retirer
	 * 			'B': recherche binaire, retrait dans un tableau ordonné
	 * 			'S': recherche séquentielle dans un tableau ordonné
	 * 			'N': recherche séquentielle dans un tableau non ordonné
	 * @return Nombre d'éléments restants à la suite du retrait
	 */
	public static int retrait(long[] vec, int nbElem, long elem, char typeRech) {
		int posItem;
		switch (typeRech) { // Recherche de l'élément à retirer
		case 'B': // Bianire
			posItem = rechercheBin(vec, nbElem, elem);
			break;
		case 'S': // Séquentielle ordonnée
			posItem = rechercheSeq(vec, nbElem, elem, true);
			break;
		default: // Séquentielle non ordonnée
			posItem = rechercheSeq(vec, nbElem, elem, false);
			break;
		}
		// TODO: Retirer l'élément s'il a été trouvé dans le vecteur
		// TODO: Traiter les cas ordonnés (B et S) et non ordonné (N)
		// TODO: Tests unitaires
		return nbElem;
	}
	
	/**
	 * Permet de faire la fusion de 2 tableaux et met le résultat dans vecF
	 * NOTE: vecF doit être initialisé et avoir une taille suffisante.
	 * 
	 * @param vecA Premier vecteur à fusionner
	 * @param nbElemA Nombre d'éléments dans le premier vecteur
	 * @param vecB Second vecteur à fusionner
	 * @param nbElemB Nombre d'éléments dans le deuxième vecteur
	 * @param vecF Vecteur recevant le résultat de la fusion
	 * @param ordonne Fusion de vecteurs ordonnés ou non
	 * @return Nombre d'éléments dans le vecteur fusionné, -1 si erreur
	 */
	public static int fusionVec(long[] vecA, int nbElemA, long[] vecB, int nbElemB, 
			long[] vecF, boolean ordonne) {
		int nbElemF = -1;
		// TODO: Vérifier si le vecF contient assez d'espace pour la fusion
		// TODO: Traiter le cas des vecteurs ordonnés et non ordonnés
		// TODO: Tests unitaires
		// Attention: nbElemF n'est pas la taille de F, ni celle de vecA + vecB,
		// mais le nombre d'éléments dans le vecteur fusionné
		return nbElemF;
	}
}
