package mcknightka.at05;


import static org.junit.Assert.assertEquals;
import org.junit.Test;//pour tester avec vscode

public class At05Ex01Test {



    @Test
    public void rechercheBinTest() {
        long testArr[] = {3, 5, 6, 8, 9, 12, 14, 17, 23, 45};
        assertEquals("essayer un nombre plus petit que la moitie", 2, At05Ex01.rechercheBin(testArr, testArr.length, 6));
    }
}
