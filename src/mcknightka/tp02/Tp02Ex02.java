package mcknightka.tp02;

import java.util.*;

public class Tp02Ex02 {	
	public static final byte NB_JOURS_SEMAINE = 7;
	
	public static void main(String[] args) {
		Scanner clavier = new Scanner(System.in);
		char choix;
		int[][] unMois = null;
		int mois = -1, annee = -1;
		int jourSem = 0, posRel = 0, leJour;
		
		do {
			System.out.print("MENU PRINCIPAL");
			if (mois > 0) {
				System.out.print(" (" + mois + "-" + annee + ")");
			}
			System.out.println();
			System.out.println("----------------------------");
			System.out.println("G - Génère un mois");
			if (mois > 0) {
				System.out.println("M - Affiche le mois généré");
				System.out.println("S - Nombre de semaines complètes");
				System.out.println("F - Nombre de fins de semaine complètes");
				System.out.println("R - Trouve la date d'un jour relatif");
			}
			System.out.println("A - Affiche le calendrier annuel (BONUS)");
			System.out.println("Q - Quitter");
			System.out.print("--> Votre choix : ");
			choix = clavier.next().toUpperCase().charAt(0);
			switch (choix) {
			case 'G':
				System.out.print("Indiquez le numéro du mois suivi de l'année: ");
				mois = clavier.nextInt();
				annee = clavier.nextInt();
				unMois = genereMois(mois, annee);
				break;
			case 'M':
				afficheMois(unMois);
				break;
			case 'S':
				System.out.println("Nombre de semaines : " + nbSemaines(unMois));
				break;
			case 'F':
				System.out.println("Nombre de fins de semaine : " + nbFinsSemaine(unMois));
				break;
			case 'R':
				System.out.print("Indiquez le jour de la semaine \n"
						+ "(0:dimanche, 1:lundi, ... 6:samedi) : ");
				jourSem = clavier.nextInt();
				System.out.print("Indiquez la position relative du jour \n"
						+ "(1:premier, 2:deuxième, -1:dernier, etc) : ");
				posRel = clavier.nextInt();
				leJour = trouveJour(unMois, jourSem, posRel);
				if (leJour > 0) {
					System.out.println("Le jour recherché est le " + leJour);
				} else {
					System.out.println("Aucun jour trouvé avec ces spécifications!");
				}
				break;
			case 'A':
				System.out.print("Indiquez l'année voulue : ");
				afficheAnnee(clavier.nextInt());
				break;
			}
			System.out.println("----------------------------");
		} while (choix != 'Q');
		System.out.println("Au revoir!");
	}
	
	/**
	 * Permet de trouver le jour de la semaine correspondant à une date donnée
	 * @param jour Jour de la date (1 - 31)
	 * @param mois Mois de la date (1 - 12)
	 * @param annee Année de la date (XXXX)
	 * @return Jour de la semaine correspondant (0:dimanche, ..., 6:samedi)
	 */
	public static int jourSemaine(int jour, int mois, int annee) {
		int jourSemaine, z;
		if (mois <= 2) {
			z = annee - 1;
			jourSemaine = (23*mois/9 + jour + 4 + annee + z/4 - z/100 + z/400) % 7;
		} else {
			z = annee;
			jourSemaine = (23*mois/9 + jour + 4 + annee + z/4 - z/100 + z/400 - 2) % 7;
		}
		return jourSemaine;
	}
	
	/**
	 * Calcule le nombre de jours pour un mois donné
	 * @param mois Mois recherché
	 * @param annee Année, car février peut être bissextile
	 * @return Nombre de jours dans le mois
	 */
	public static int joursParMois(int mois, int annee) {
		int[] nbJours = {31,28,31,30,31,30,31,31,30,31,30,31};
		if (annee % 4 == 0 && annee % 100 != 0 || annee % 400 == 0) {
			nbJours[1]++;
		}
		return nbJours[mois-1];
	}
	
	/**
	 * Crée un tableau 2D contenant la liste des jours du mois demandé
	 * en paramètre.  Le tableau contient une colonne pour chaque jour de
	 * la semaine et autant de lignes que nécessaire pour stocker tous les
	 * jours du mois.  La première colonne (indice 0) correspond à dimanche
	 * @param mois Numéro du mois à générer (1 à 12)
	 * @param annee Année du mois à générer
	 * @return Tableau contenant les dates du mois ou 0 pour les jours sans date
	 */
	public static int[][] genereMois(int mois, int annee) {
		int premierJour = jourSemaine(1, mois, annee);
		int joursMois = joursParMois(mois, annee);
		int nbSem = (int) Math.ceil((joursMois + premierJour)/7.0f);
		int[][] cal = new int[nbSem][NB_JOURS_SEMAINE];
		int index = premierJour;
		for (int i = 1; i <= joursMois; ++i, ++index) {
			cal[index/NB_JOURS_SEMAINE][index%NB_JOURS_SEMAINE] = i;
		}
		return cal;
	}

	/**
	 * Affiche le calendrier du mois reçu en paramètre avec
	 * les jours de la semaine en en-tête
	 * @param calMois Tableau/calendrier du mois à afficher
	 */
	public static void afficheMois(int[][] calMois) {
		// TODO: Afficher le calendrier à la console en  
		// respectant le format de l'exemple
// Exemple d'affichage pour avril 2009:
// Di  Lu  Ma  Me  Je  Ve  Sa
// --------------------------
//              1   2   3   4  
//  5   6   7   8   9  10  11  
// 12  13  14  15  16  17  18  
// 19  20  21  22  23  24  25  
// 26  27  28  29  30          

        System.out.println("Di  Lu  Ma  Me  Je  Ve  Sa");
        System.out.println("--------------------------");
        
        for (int i = 0; i < calMois.length; i++) {
            for (int j = 0; j < calMois[i].length; j++) {
                String num = "";
                if (calMois[i][j] == 0) {
                    num = "    ";
                }else {
                    num = calMois[i][j] + "  ";
                }

                if (calMois[i][j] < 10 && calMois[i][j] > 0) {
                    num = " " + num;
                }

                System.out.print(num);
            }

            System.out.println("");
        }



	}

	/**
	 * Calcule le nombre de semaines complètes dans le mois
	 * Une semaine est complète lorsque son lundi et son 
	 * vendredi se retrouvent dans le même mois
	 * @param calMois Tableau/calendrier du mois courant
	 * @return Nombre de semaines complètes
	 */
	public static int nbSemaines(int[][] calMois) {
		int compte = 0;
		
        for (int i = 0; i < calMois.length; i++) {
            if(calMois[i][0] > 0 && calMois[i][6] > 0){
                compte ++;
            }
        }


		return compte;
	}

	/**
	 * Calcule le nombre de fins de semaine complètes dans le mois
	 * Une fin de semaine est complète lorsque le samedi et le 
	 * dimanche se trouvent dans le même mois
	 * @param calMois Tableau/calendrier du mois courant
	 * @return Nombre de fins de semaine complètes
	 */
	public static int nbFinsSemaine(int[][] calMois) {
		int compte = 0;

		for (int i = 0; i < calMois.length - 1; i++) {
            if(calMois[i + 1][0] > 0 && calMois[i][6] > 0){
                compte ++;
            }
        }

		return compte;
	}

	/**
	 * Fonction qui recherche la date du jour correspondant  
	 * à la position realtive reçue en paramètre
	 * @param calMois Tableau/calendrier du mois dans lequel faire la recherche
	 * @param jour Jour de la semaine recherché (0:dimanche, ..., 6:samedi)
	 * @param pos Position relative du jour (1: premier, 2: second, -1:dernier, etc)
	 * @return La date du jour recherché ou -1 si non trouvé
	 */
	public static int trouveJour(int[][] calMois, int jour, int pos) {
		int jourTrouve = -1;
		// TODO: Trouver le jour correspondant à la spécification
		// Exemple: Le premier lundi d'avril 2009 est le 6 (jour=1, pos=1)
		// Exemple: L'avant dernier vendredi d'avril 2009 est le 17 (jour=5, pos=-2)
		// Traiter séparément les pos positives et négatives
		// TODO: Ecrire au moins cinq tests unitaires distincts

		if(pos < 0){
			pos = calMois.length + pos;
		}

		if (calMois[pos][jour] != 0) {
			jourTrouve = calMois[pos][jour];
		}


		return jourTrouve;
	}

	/**
	 * Affiche le calendrier de chacun des mois d'une année sous 
	 * forme d'une table de quatre lignes de trois mois.
	 * @param annee Année à afficher
	 */
	public static void afficheAnnee(int annee) {
		// TODO: BONUS: Afficher le calendrier d'une année en format 4x3
		// Appeler la fonction aff3Mois() pour chaque "ligne"
// Exemple d'affichage pour l'année 2009:
// Janvier 2009                  Février 2009                  Mars 2009                     
// Di  Lu  Ma  Me  Je  Ve  Sa    Di  Lu  Ma  Me  Je  Ve  Sa    Di  Lu  Ma  Me  Je  Ve  Sa
// --------------------------    --------------------------    --------------------------
//                  1   2   3     1   2   3   4   5   6   7     1   2   3   4   5   6   7  
//  4   5   6   7   8   9  10     8   9  10  11  12  13  14     8   9  10  11  12  13  14  
// 11  12  13  14  15  16  17    15  16  17  18  19  20  21    15  16  17  18  19  20  21  
// 18  19  20  21  22  23  24    22  23  24  25  26  27  28    22  23  24  25  26  27  28  
// 25  26  27  28  29  30  31                                  29  30  31                  
//
// Avril 2009                    Mai 2009                      Juin 2009                     
// Di  Lu  Ma  Me  Je  Ve  Sa    Di  Lu  Ma  Me  Je  Ve  Sa    Di  Lu  Ma  Me  Je  Ve  Sa
// --------------------------    --------------------------    --------------------------
//              1   2   3   4                         1   2         1   2   3   4   5   6  
//  5   6   7   8   9  10  11     3   4   5   6   7   8   9     7   8   9  10  11  12  13  
// 12  13  14  15  16  17  18    10  11  12  13  14  15  16    14  15  16  17  18  19  20  
// 19  20  21  22  23  24  25    17  18  19  20  21  22  23    21  22  23  24  25  26  27  
// 26  27  28  29  30            24  25  26  27  28  29  30    28  29  30                  
//                               31                                                        
//
// Juillet 2009                  Août 2009                     Septembre 2009                
// Di  Lu  Ma  Me  Je  Ve  Sa    Di  Lu  Ma  Me  Je  Ve  Sa    Di  Lu  Ma  Me  Je  Ve  Sa
// --------------------------    --------------------------    --------------------------
//              1   2   3   4                             1             1   2   3   4   5  
//  5   6   7   8   9  10  11     2   3   4   5   6   7   8     6   7   8   9  10  11  12  
// 12  13  14  15  16  17  18     9  10  11  12  13  14  15    13  14  15  16  17  18  19  
// 19  20  21  22  23  24  25    16  17  18  19  20  21  22    20  21  22  23  24  25  26  
// 26  27  28  29  30  31        23  24  25  26  27  28  29    27  28  29  30              
//                               30  31                                                    
//
// Octobre 2009                  Novembre 2009                 Décembre 2009                 
// Di  Lu  Ma  Me  Je  Ve  Sa    Di  Lu  Ma  Me  Je  Ve  Sa    Di  Lu  Ma  Me  Je  Ve  Sa
// --------------------------    --------------------------    --------------------------
//                  1   2   3     1   2   3   4   5   6   7             1   2   3   4   5  
//  4   5   6   7   8   9  10     8   9  10  11  12  13  14     6   7   8   9  10  11  12  
// 11  12  13  14  15  16  17    15  16  17  18  19  20  21    13  14  15  16  17  18  19  
// 18  19  20  21  22  23  24    22  23  24  25  26  27  28    20  21  22  23  24  25  26  
// 25  26  27  28  29  30  31    29  30                        27  28  29  30  31          
	}
	
	/**
	 * Affiche les calendriers de trois mois côte à côte (sur une même "ligne")
	 * Le nombre de lignes à réserver pour l'affichage correspond au nombre
	 * maximum de semaines parmi les trois calendriers
	 * @param c1 Tableau/calendrier du premier mois à afficher
	 * @param c2 Tableau/calendrier du second mois à afficher
	 * @param c3 Tableau/calendrier du troisième mois à afficher
	 */
	public static void aff3Mois(int[][] c1, int[][] c2, int[][] c3) {
		// TODO: BONUS -  Afficher une "ligne" de trois calendriers
// Exemple d'affichage:
// Di  Lu  Ma  Me  Je  Ve  Sa    Di  Lu  Ma  Me  Je  Ve  Sa    Di  Lu  Ma  Me  Je  Ve  Sa
// --------------------------    --------------------------    --------------------------
//                  1   2   3     1   2   3   4   5   6   7     1   2   3   4   5   6   7  
//  4   5   6   7   8   9  10     8   9  10  11  12  13  14     8   9  10  11  12  13  14  
// 11  12  13  14  15  16  17    15  16  17  18  19  20  21    15  16  17  18  19  20  21  
// 18  19  20  21  22  23  24    22  23  24  25  26  27  28    22  23  24  25  26  27  28  
// 25  26  27  28  29  30  31                                  29  30  31                  
	}
}
