package mcknightka.tp02;


import static org.junit.Assert.*;
import org.junit.Test;//pour tester avec vscode

public class Tp02Ex02Test {

    @Test
    public void nbFinsSemaine () {
        
        
        int arr[][] = {{
            0, 0, 0, 0, 0, 0, 1
        }, {
            1, 0, 0, 0, 0, 0, 1
        }, {
            1, 0, 0, 0, 0, 0, 1
        }, {
            1, 0, 0, 0, 0, 0, 1
        }, {
            0, 0, 0, 0, 0, 0, 0
        }, {
            0, 0, 0, 0, 0, 0, 0
        }};
        assertEquals("Une fin de semaine incomplete", 3, Tp02Ex02.nbFinsSemaine(arr));
        
        
        int arr1[][] = {{
            0, 0, 0, 0, 0, 0, 3
        }, {
            3, 0, 0, 0, 0, 0, 3
        }, {
            3, 0, 0, 0, 0, 0, 3
        }, {
            3, 0, 0, 0, 0, 0, 3
        }, {
            0, 0, 0, 0, 0, 0, 0
        }, {
            0, 0, 0, 0, 0, 0, 0
        }};
        assertEquals("Une fin de semaine completement vide", 3, Tp02Ex02.nbFinsSemaine(arr1));
        
        
        int arr2[][] = {{
            0, 0, 0, 0, 0, 0, 0
        }, {
            1, 0, 0, 0, 0, 0, 1
        }, {
            1, 0, 0, 0, 0, 0, 1
        }, {
            1, 0, 0, 0, 0, 0, 1
        }, {
            0, 0, 0, 0, 0, 0, 0
        }, {
            0, 0, 0, 0, 0, 0, 0
        }};
        assertEquals("deux fin de semaines incompletes", 2, Tp02Ex02.nbFinsSemaine(arr2));
        
        
        int arr3[][] = {{
            0, 0, 0, 0, 0, 0, 1
        }, {
            2, 3, 4, 5, 6, 7, 8
        }, {
            9, 10, 11, 12, 13, 14, 15,
        }, {
            16, 17, 18, 19, 20, 21, 22
        }, {
            23, 24, 25, 26, 27, 28, 29
        }, {
            30, 31, 0, 0, 0, 0, 0
        }};
        assertEquals("une structure de mois au complet (janvier 2000)", 5, Tp02Ex02.nbFinsSemaine(arr3));
        
        
        int arr4[][] = {{
            0, 0, 0, 0, 0, 0, 4
        }, {
            4, 0, 0, 0, 0, 0, 4
        }, {
            4, 0, 0, 0, 0, 0, 4
        }, {
            4, 0, 0, 0, 0, 0, 4
        }, {
            4, 0, 0, 0, 0, 0, 4
        }, {
            4, 0, 0, 0, 0, 0, 0
        }};
        assertEquals("5 fins de semaines completes (le maximum)", 5, Tp02Ex02.nbFinsSemaine(arr4));

    }

    @Test
    public void trouveJourTest () {

        int arr[][] = {{
            0, 0, 0, 0, 0, 0, 1
        }, {
            2, 3, 4, 5, 6, 7, 8
        }, {
            9, 10, 11, 12, 13, 14, 15,
        }, {
            16, 17, 18, 19, 20, 21, 22
        }, {
            23, 24, 25, 26, 27, 28, 29
        }, {
            30, 31, 0, 0, 0, 0, 0
        }};
        assertEquals("Jour qui n'existe pas", -1, Tp02Ex02.trouveJour(arr, 0, 0));


        assertEquals("Premiere journee du mois", 1, Tp02Ex02.trouveJour(arr, 6, 0));


        assertEquals("derniere journee du mois", 31, Tp02Ex02.trouveJour(arr, 1, -1));

        assertEquals("utilisation d'une valeur negative", 20, Tp02Ex02.trouveJour(arr, 4, -3));

        assertEquals("entree normale", 25, Tp02Ex02.trouveJour(arr, 2, 4));

    }

    
}
