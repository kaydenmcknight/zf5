package mcknightka.tp02;

import java.util.*;

import commun.Outils;

public class Tp02Ex01 {
	public static void main(String[] args) {
		Scanner clavier = new Scanner(System.in);
		Outils.Aleatoire.setSeed(13579);

		System.out.print("Entrez la valeur maximale : ");
		int valMax = clavier.nextInt();
		System.out.print("Inscrivez le nombre de valeurs: ");
		int nbVal = clavier.nextInt();
		int[] vecOri = genereVecAleatoire(nbVal, valMax);
		int[] vecImpairs = extraitValeurs(vecOri, false);
		int[] vecPairs = extraitValeurs(vecOri, true);
		System.out.print("Vecteur initial: ");
		Outils.println(vecOri);
		System.out.print("Vecteur pairs:   ");
		Outils.println(vecPairs);
		System.out.print("Vecteur impairs: ");
		Outils.println(vecImpairs);
		System.out.println();

		triSel(vecPairs);
		triSel(vecImpairs);
		int[] vecFusion = fusionVec(vecPairs, vecImpairs);
		System.out.println("Après le tri de chaque vecteur:");		
		System.out.print("Vecteur pairs:   ");
		Outils.println(vecPairs);
		System.out.print("Vecteur impairs: ");
		Outils.println(vecImpairs);
		System.out.print("Vecteur fusion:  ");
		Outils.println(vecFusion);
		System.out.println();
		
		System.out.print("Quelle est la valeur recherchée? ");
		int valCherchee = clavier.nextInt();
		int posVal = rechercheBin(vecFusion, valCherchee);
		if (posVal!=-1) {
			System.out.println("La valeur recherchée se trouve en position " + posVal);
		} else {
			System.out.print("Valeur introuvable");
		}
	}
	
	/**
	 * Permet de générer un vecteur de nombres aléatoires
	 * @param taille Taille du vecteur à générer
	 * @param valMax Valeur maximale des nombres générés.
	 * 			Ils seront entre 0 inclusif et valMax exclusif.
	 * @return Vecteur contenant les nombres générés
	 */
	public static int[] genereVecAleatoire(int taille, int valMax) {
		int[] vecA = new int[taille];
		for (int i = 0; i < taille; ++i) {
			vecA[i] = Outils.Aleatoire.nextInt(valMax);
		}
		return vecA;
	}

	/**
	 * Crée un nouveau vecteur contenant seulement les valeurs paires ou
	 * impaires à partir du vecteur reçu en paramètre
	 * La taille du nouveau vecteur doit correspondre au nombre de valeurs extraites
	 * @param vecNb Vecteur de départ
	 * @param pair Sélectionne les valeurs paires (true) ou impaires (false)
	 * @return Nouveau vecteur contenant les valeurs extraites
	 */
	public static int[] extraitValeurs(int[] vecNb, boolean pair) {
        int nbValeurs = 0;

        for (int i = 0; i < vecNb.length; i++) {
            if(vecNb[i] % 2 == 0 && pair){
                nbValeurs ++;
            }else if(!(vecNb[i] % 2 == 0) && !pair){
                nbValeurs ++;
            }
        }

		int[] nbExtraits = new int[nbValeurs];
        int nbExtractions = 0;

        for (int i = 0; i < vecNb.length; i++) {
            if(vecNb[i] % 2 == 0 && pair){
                nbExtraits[nbExtractions] = vecNb[i];
                nbExtractions ++;
            }else if(!(vecNb[i] % 2 == 0) && !pair){
                nbExtraits[nbExtractions] = vecNb[i];
                nbExtractions ++;
            }
        }

		return nbExtraits;
	}
	
	/**
	 * Effectue un tri par sélection du vecteur reçu en paramètre
	 * @param vec Vecteur qui sera trié
	 */
	public static void triSel(int[] vec) {
		
        for (int i = 0; i < vec.length; i++) {
            int indPlusPetit = i;

            for (int j = i; j < vec.length; j++) {
                if (vec[j] < vec[indPlusPetit]) {
                    indPlusPetit = j;
                }

            }

            int temp = vec[i];
            vec[i] = vec[indPlusPetit];
            vec[indPlusPetit] = temp;
        }
	}

	/**
	 * Fusionne deux vecteurs triés.  La taille du vecteur résultant
	 * est la somme des tailles des deux vecteurs reçus en paramètres
	 * @param vecA Premier vecteur trié à fusionner
	 * @param vecB Second vecteur trié à fusionner
	 * @return Vecteur trié résultant de la fusion de vecA et vecB
	 */
	public static int[] fusionVec(int[] vecA, int[] vecB) {
		int[] vecF = new int[vecA.length + vecB.length];

        for (int i = 0; i < vecF.length; i++) {
            if (i < vecA.length) {
                vecF[i] = vecA[i]; 
            }else {
                vecF[i] = vecB[i - vecA.length];
            }
        }

        triSel(vecF);
		
		return vecF;
	}
	
	/**
	 * Recherche binaire d'une valeur dans un vecteur trié
	 * @param vec Vecteur trié dans lequel la valeur sera recherchée
	 * @param valeur Valeur à rechercher
	 * @return Position de la valeur dans le vecteur ou -1 si non trouvée
	 */
	public static int rechercheBin(int[] vec, int valeur) {
		int posGa = 0;
		int posDr = vec.length - 1;
		int posMi = -1;
		int resultat = -1;

        while (posGa < posDr) {
            posMi = (posGa + posDr)/2;

            if (valeur > vec[posMi]) {
                posDr = posMi;
            }else if (valeur < vec[posMi]) {
                posGa = posMi;
            }else {
                resultat = posMi;
                posDr = -1;
            }
        }


		return posMi;
	}
}
