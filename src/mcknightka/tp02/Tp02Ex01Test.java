package mcknightka.tp02;


import static org.junit.Assert.*;
import org.junit.Test;//pour tester avec vscode

public class Tp02Ex01Test {

    @Test
    public void extraitValeursTest (){

        int arr[] = {5, 3, 2, 2, 1, 4, 6};
        int rslt[] = {2, 2, 4, 6};
        assertArrayEquals("Test avec valeurs regulieres", rslt, Tp02Ex01.extraitValeurs(arr, true));
        int arr1[] = {2, 4, 6, 8, 8, 4, 6, 22};
        int rslt1[] = new int[0];
        assertArrayEquals("Test avec seulement des valeurs paires", rslt1, Tp02Ex01.extraitValeurs(arr1, false));
        int arr2[] = {5, 3, 7, 9, 1, 1, 3};
        int rslt2[] = {5, 3, 7, 9, 1, 1, 3};
        assertArrayEquals("Test avec seulement des valeurs impaires en cherchat pour eux", rslt2, Tp02Ex01.extraitValeurs(arr2, false));
        int arr3[] = {5, 3, 2, 2, 1, 4, 6};
        int rslt3[] = {5, 3, 1};
        assertArrayEquals("Test avec valeurs regulieres, retourne les impairs", rslt3, Tp02Ex01.extraitValeurs(arr3, false));
        int arr4[] = {2, 4, 6, 8, 8, 4, 6, 22};
        int rslt4[] = new int[0];
        assertArrayEquals("Test avec seulement des valeurs paires en cherchant pour des imapires", rslt4, Tp02Ex01.extraitValeurs(arr4, false));

    }



    @Test
    public void triSelTest (){

        int arr[] = {5, 3, 2, 2, 2, 1};
        Tp02Ex01.triSel(arr); // test avec plusieurs nombres identiques
        System.out.println("Test #1 devrait etre \"1 2 2 2 3 5\", donne un resultat de " + concat(arr));
        
        int arr1[] = {2, 4, 6, 8, 8, 4, 6, 22};
        Tp02Ex01.triSel(arr1);//test avec nombres reguilers
        System.out.println("Test #2 devrait etre \"2 4 4 6 6 22\", donne un resultat de " + concat(arr1));
        
        int arr2[] = {44, 1};
        Tp02Ex01.triSel(arr2);//test avec tavleu a juste 2 numeros
        System.out.println("Test #3 devrait etre \"1 44\", donne un resultat de " + concat(arr2));
        
        int arr3[] = {1, 2, 3, 4, 5, 6};
        Tp02Ex01.triSel(arr3);//test avec un tableau deja organise
        System.out.println("Test #4 devrait etre \"1 2 3 4 5 6\", donne un resultat de " + concat(arr3));
        
        int arr4[] = {22, 44, 66, 55, 77, 88};
        Tp02Ex01.triSel(arr4);//test avec seulement 2 muneros a changer
        System.out.println("Test #5 devrait etre \"22 44 55 66 77 88\", donne un resultat de " + concat(arr4));

    }

    public String concat(int[] arr){
        String result = arr[0] + "";
        for (int i = 1; i < arr.length; i++) {
            result += " " + arr[i];
        }

        return result;
    }

    
}
