package mcknightka.at04;

import static org.junit.Assert.assertEquals;

import org.junit.Test;//pour tester avec vscode

public class at04Test {
    



    @Test
    public void noteEnEtoilesTest () {
        assertEquals("Essayer un nombre standard", "****", at04.noteEnEtoiles(4f));
        assertEquals("Essayer un nombre au dessus de .75", "*", at04.noteEnEtoiles(1.8f));
        assertEquals("Essayer un nombre en dessous de .25", "****", at04.noteEnEtoiles(4.23f));
        assertEquals("Essayer un nombre entre .25 et .75", "****·", at04.noteEnEtoiles(4.5f));
        assertEquals("Essayer un 0", "", at04.noteEnEtoiles(0f));
    }

    @Test
    public void calculMoyenneTest () {
        int temp[] = {2, 2, 3, 4};
        assertEquals("Essayer un nombre aleatoire, il n'as pas vraiment de difference dans une moyenne", 2.75f, at04.calculMoyenne(temp), 0);
        int temp1[] = {7, 6, 4, 8};
        assertEquals("Essayer un nombre aleatoire, il n'as pas vraiment de difference dans une moyenne", 6.25f, at04.calculMoyenne(temp1), 0);
        int temp2[] = {4, 4, 5, 6};
        assertEquals("Essayer un nombre aleatoire, il n'as pas vraiment de difference dans une moyenne", 4.75f, at04.calculMoyenne(temp2), 0);
        int temp3[] = {5, 0, 4, 4};
        assertEquals("Essayer un nombre aleatoire, il n'as pas vraiment de difference dans une moyenne", 3.25f, at04.calculMoyenne(temp3), 0);
        int temp4[] = {2, 2, 2};
        assertEquals("Essayer un nombre aleatoire, il n'as pas vraiment de difference dans une moyenne", 2f, at04.calculMoyenne(temp4), 0);
    }

    @Test
    public void calculMoyenneFloatTest () {
        float temp[] = {2, 5, 3, 5, 3.4f, 4};
        assertEquals("Essayer un nombre aleatoire, il n'as pas vraiment de difference dans une moyenne", 3.7333333f, at04.calculMoyenneFloat(temp), 0);
        float temp1[] = {7, 6.3f, 4, 8};
        assertEquals("Essayer un nombre aleatoire, il n'as pas vraiment de difference dans une moyenne", 6.325f, at04.calculMoyenneFloat(temp1), 0);
        float temp2[] = {4, 4.2f, 5, 6};
        assertEquals("Essayer un nombre aleatoire, il n'as pas vraiment de difference dans une moyenne", 4.8f, at04.calculMoyenneFloat(temp2), 0);
        float temp3[] = {5, 0.1f, 4, 4};
        assertEquals("Essayer un nombre aleatoire, il n'as pas vraiment de difference dans une moyenne", 3.275f, at04.calculMoyenneFloat(temp3), 0);
        float temp4[] = {2, 2.2f, 2};
        assertEquals("Essayer un nombre aleatoire, il n'as pas vraiment de difference dans une moyenne", 2.0666666f, at04.calculMoyenneFloat(temp4), 0);
    }

    @Test
    public void trouvePosMinTest () {
        int temp[] = {2, 5, 3, 6};
        assertEquals("Essais de valuers aleatoires, pas beaucoup de difference", 0, at04.trouvePosMin(temp));
        int temp1[] = {1, 7, 3, 2};
        assertEquals("Essais de valuers aleatoires, pas beaucoup de difference", 0, at04.trouvePosMin(temp1));
        int temp2[] = {7, 5, 3, 8};
        assertEquals("Essais de valuers aleatoires, pas beaucoup de difference", 2, at04.trouvePosMin(temp2));
        int temp3[] = {3, 2};
        assertEquals("Essais de valuers aleatoires, pas beaucoup de difference", 1, at04.trouvePosMin(temp3));
        int temp4[] = {2, 2, 2};
        assertEquals("Essais de valuers aleatoires, pas beaucoup de difference", 0, at04.trouvePosMin(temp4));
    }

    @Test
    public void trouvePosMaxTest () {
        int temp[] = {2, 5, 3, 6};
        assertEquals("Essais de valuers aleatoires, pas beaucoup de difference", 3, at04.trouvePosMax(temp));
        int temp1[] = {1, 7, 3, 2};
        assertEquals("Essais de valuers aleatoires, pas beaucoup de difference", 1, at04.trouvePosMax(temp1));
        int temp2[] = {7, 5, 3, 8};
        assertEquals("Essais de valuers aleatoires, pas beaucoup de difference", 3, at04.trouvePosMax(temp2));
        int temp3[] = {4, 2};
        assertEquals("Essais de valuers aleatoires, pas beaucoup de difference", 0, at04.trouvePosMax(temp3));
        int temp4[] = {2, 2, 2};
        assertEquals("Essais de valuers aleatoires, pas beaucoup de difference", 0, at04.trouvePosMax(temp4));
    }
}
    
    
