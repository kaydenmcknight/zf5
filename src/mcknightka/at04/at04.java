package mcknightka.at04;

import commun.MenuMaker;

public class at04 {
    
    public static void main(String[] args) {
        MenuMaker m = new MenuMaker(20);
        int nb = m.promptInt("Indiquez le nombre de recettes à évaluer: ");
        String recettes[] = new String[nb];
        int temps[] = new int[nb];
        int evaluateurs;

        demandeRecettes(recettes, temps);
        

        m.line(20);
        m.right("T", "Recette");
        m.line(20);
        for (int i = 0; i < temps.length; i++) {
            m.right(temps[i] + "", recettes[i]);
        }
        m.line(20);
        m.right(calculMoyenne(temps) + "", "Moyenne");

        m.print("Temps de préparation le plus long : " + recettes[trouvePosMax(temps)], false);
        m.print("Temps de préparation le plus court : " + recettes[trouvePosMin(temps)], false);


        m.wt("Appuyez sur \"enter\" pour continuer..");

        evaluateurs = m.promptInt("Indiquez le nombre d'évaluateurs : ");
        float evaluations[][] = new float[recettes.length][evaluateurs];
        for (int i = 0; i < recettes.length; i++) {
            m.print("Entrez les notes (0 à 5) pour " + recettes[i] + " :");
            for (int j = 0; j < evaluateurs; j++) {
                evaluations[i][j] = m.promptFloat("", false);
            }
        }

        int columnWidths[] = new int[2 + evaluateurs];//{22, 8, };
        columnWidths[0] = 22;
        for (int i = 1; i < columnWidths.length; i++) {
            columnWidths[i] = 8;
        }
        String data[][] = new String[recettes.length + 2][2 + evaluateurs];

        data[0][0] = "Recette";
        for (int i = 0; i < evaluations.length; i++) {
            data[0][i + 1] = "E#" + (i + 1);
        }
        data[0][evaluateurs + 1] = "Moy";
    

        for (int i = 0; i < recettes.length; i++) {//pour toutes les recettes
            data[i + 1][0] = recettes[i];
            for (int j = 0; j < evaluateurs; j++) {//pour toutes les evaluations
                data[i + 1][j + 1] = noteEnEtoiles(evaluations[i][j]);
            }

            data[i + 1][evaluations.length + 1] = calculMoyenneFloat(evaluations[i]) + "";
        }

        data[recettes.length + 1][0] = "Moyenne";
        for (int i = 0; i < evaluations[0].length; i++) {
            float temp[] = new float[recettes.length];
            
            for (int j = 0; j < temp.length; j++) {
                temp[j] = evaluations[j][i];
            }

            data[recettes.length + 1][i + 1] = calculMoyenneFloat(temp) + "";
        }

        m.cls();
        m.table(columnWidths, data);

    }

    /**
     * Retourne la note en etoiles
     * @param note La note a transformer
     * @return La note en format etoile
     */
    public static String noteEnEtoiles(float note) {
        String resultat = "";

        for (int i = 0; i < note + 1; i++) {
            if (note > i && note >= (i + 1)) {//si le nombre est pas une demi
                resultat += "*";
            }else if (note > i + 0.25 && note < i + 0.75){
                resultat += "·";
            }
        }

        return resultat;
    }

    /**
     * Demande les recettes  en dependant du montant (les resultats sont mis dans les parametres)
     * @param recettes tableau vide (grandeur determine le nombre de recettes a demander)
     * @param temps tableau de la meme grandeur que recettes, va contenir les donnes de temps de chaque recette
     */
    public static void demandeRecettes(String[] recettes, int[] temps){
        MenuMaker m = new MenuMaker();
        for (int i = 0; i < temps.length; i++) {
            recettes[i] = m.prompt("Entrez le nom de la recette #" + (i + 1) + " : ");
            temps[i] = m.promptInt("Inscrivez le temps de préparation: ");

        }
    }

    /**
     * Calcule et retourne la moyenne
     * @param vecteur tableau de numeros a calculer 
     * @return la valeur de la moyenne de ceux ci
     */
    public static float calculMoyenne(int[] vecteur) {
        float resultat = 0;

        for (int i = 0; i < vecteur.length; i++) {
            resultat += vecteur[i];
        }

        resultat = resultat / vecteur.length;


        return resultat;
    }

    /**
     * Calcule la moyenne mais prends des parametres en float
     * @param vecteur tableau des parametres a calculer
     * @return la moyenne du calcul
     */
    public static float calculMoyenneFloat(float[] vecteur) {
        float resultat = 0;

        for (int i = 0; i < vecteur.length; i++) {
            resultat += vecteur[i];
        }

        resultat /= vecteur.length;


        return resultat;
    }

    /**
     * Trouve l'index de la recette qui prends le moins de temps
     * @param vecteur liste de temps des recettes
     * @return index de la recette qui prends le moins de temps
     */
    public static int trouvePosMin(int[] vecteur) {
        int resultat = 0;
        int temps = Integer.MAX_VALUE;
        for (int i = 0; i < vecteur.length; i++) {
            if (temps > vecteur[i]) {
                temps = vecteur[i];
                resultat = i;
            }
        }
        return resultat;
    }

    /**
     * Trouve l'index de la recette qui prends le plus de temps
     * @param vecteur liste de temps des recettes
     * @return index de la recette qui prends le plus de temps
     */
    public static int trouvePosMax(int[] vecteur) {
        int resultat = 0;
        int temps = 0;
        for (int i = 0; i < vecteur.length; i++) {
            if (temps < vecteur[i]) {
                temps = vecteur[i];
                resultat = i;
            }
        }
        return resultat;
    }
    
    
}
