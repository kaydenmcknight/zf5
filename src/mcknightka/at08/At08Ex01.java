package mcknightka.at08;

import java.lang.reflect.*;
import java.util.*;

public class At08Ex01 {

	public static void main(String[] args) {
		Scanner cl = new Scanner(System.in);
		String[][] infosForm = {
				{"ID formulaire", "valideIDForm", "Inscrivez l'identificateur du formulaire (REV-#####/##)", ""},
				{"Nom, Prénom", "valideNom", "Indiquez le nom de famille et le prénom de l'étudiant", ""},
				{"Code MEQ", "valideMEQ", "Indiquez le code du MEQ de l'étudiant (AAAA #### ####)", ""},
				{"Matricule", "valideMatricule", "Inscrivez le numéro de matricule de l'étudiant", ""}, 
				{"Trimestre", "valideTrimestre", "Inscrivez le trimestre (ex:A2020)", ""},
				{"Programme", "valideProgramme", "Indiquez le programme d'étude", ""}, 
				{"Sigle du cours", "valideSigle", "Indiquez le sigle du cours (###-AAA-MO)", ""}, 
				{"ID évaluation", "valideIDEval", "Identifiez l'évaluation (AA###)", ""}, 
				{"Date de l'évaluation", "valideDate", "Indiquez la date de l'évaluation (JJ/MM/AAAA)", ""}, 
				{"Note", "valideNote", "Indiquez la nouvelle note (CT = 100)", ""}
		};
		char choix;
		
		do {
			for (int i = 0; i < infosForm.length; ++i) {
				System.out.println("" + (char) ('A'+i) + " - " +infosForm[i][0] + " : " + infosForm[i][3]);
			}
			System.out.println("T - Entrer toutes les données");
			System.out.println("Q - Quitter");
			System.out.print("Votre choix > ");
			choix = cl.nextLine().toUpperCase().charAt(0);
			if (choix >= 'A' && choix < 'A' + infosForm.length) {
				infosForm[choix - 'A'][3] = demandeInfo(cl, infosForm[choix - 'A']);
			} else if (choix == 'T') {
				for (int i = 0; i < infosForm.length; ++i) {
					infosForm[i][3] = demandeInfo(cl, infosForm[i]);					
				}
			}
		} while (choix != 'Q');
		System.out.println("Bonne journée.");
	}
	
	public static String demandeInfo(Scanner clavier, String[] infos) {
		String nouvelleDonnee = null;
		boolean donneeValide = false;
		try {
			Class<At08Ex01> cl = At08Ex01.class;
			Method me = cl.getMethod(infos[1], String.class);
			System.out.print(infos[2] + " [" + infos[3] + "] : ");
			nouvelleDonnee = clavier.nextLine().trim();
			while (!nouvelleDonnee.isEmpty() && !donneeValide) {
				nouvelleDonnee = (String) me.invoke(null, nouvelleDonnee);
				if (nouvelleDonnee.isEmpty()) {
					System.out.println("Entrée invalide, SVP réessayez!");
					System.out.print(infos[2] + " [" + infos[3] + "] : ");
					nouvelleDonnee = clavier.nextLine().trim();					
				} else {
					donneeValide = true;
				}
			} 
			if (nouvelleDonnee.isEmpty()) {
				nouvelleDonnee = infos[3];				
			}			
		}
        catch (Throwable e) {
            System.err.println(e);
        }
		return nouvelleDonnee;
	}
	
	// ID Formulaire : REV-#####/##
	public static String valideIDForm(String idForm) {
		String nouvelID = "";

        if(idForm.matches("(REV[- ])?([0-9]{5})[/ -]?([0-2]((?<!2)[0-9]|[0-2]))")){
            nouvelID = idForm.replaceFirst("(REV[- ])?([0-9]{5})[/ -]?([0-2]((?<!2)[0-9]|[0-2]))", "REV-$2/$3");
        }

		return nouvelID;
	}	
	
	// Nom, Prénom : Blier, Jean-Philippe
	public static String valideNom(String nom) {
		String nouveauNom = "";

        if (nom.matches("[A-Z][a-z -]*, [A-Z][a-z -]*")){
            nouveauNom = nom;
        }
		return nouveauNom;
	}
	
	// #MEQ : LLLL #### ####
	public static String valideMEQ(String noMEQ) {
		String nouveauMEQ = "";

		if (noMEQ.matches("([A-Z]{4})[ ]?([0-9]{4})[ ]?([0-9]{4})")) {
			nouveauMEQ = noMEQ.replaceFirst("([A-Z]{4})[ ]?([0-9]{4})[ ]?([0-9]{4})", "$1 $2 $3");
		}

		return nouveauMEQ;
	}
	
	// Matricule : #######
	public static String valideMatricule(String matricule) {
		String nouveauMat = "";


		if (matricule.matches("([0-9][2])?([0-9]{7})")) {
			nouveauMat = matricule.replaceFirst("([0-9][2])?([0-9]{7})", "$2");
		}
		return nouveauMat;
	}
	
	// Trimestre (H ou Hiver + année) : A####
	public static String valideTrimestre(String trimestre) {
		String nouveauTrimestre = "";

		if (trimestre.matches("(A|Automne|H|Hiver)[- ]?((19|20)[0-9]{2})")) {
			nouveauTrimestre = trimestre.replaceFirst("(A|Automne|H|Hiver)[- ]?((19|20)[0-9]{2})", "$1$2");
		}
		return nouveauTrimestre;
	}
	
	// Programme : Chaîne de caractères
	public static String valideProgramme(String prog) {
		String nouveauProg = "";
		if (prog.matches(".*")) {
			nouveauProg = prog;
	   	}
		return nouveauProg;
	}
	
	// Sigle cours : ###-...-MO
	public static String valideSigle(String sigle) {
		String nouveauSigle = "";
		if (sigle.matches("[0-9]{3}-[0-9 A-Z]{3}-MO")) {
			nouveauSigle = sigle;
	   	}
		return nouveauSigle;
	}

	// ID évaluation : AA###
	public static String valideIDEval(String idEval) {
		String nouvelID = "";
		if (idEval.matches("[A-Z]{2}[0-9]{3}")) {
			nouvelID = idEval;
	   	}
		return nouvelID;
	}
	
	// Date évaluation : JJ/MM/AAAA
	public static String valideDate(String date) {
		String nouvelleDate = "";

		if (date.matches("((3[0-1])|([0-2][0-9]))[-/ ]?((1[0-2])|(0[0-9]))[-/ ]?((20[0-2][0-9])|([0-1][0-9]{3}))")) {
			nouvelleDate = date.replaceFirst("((3[0-1])|([0-2][0-9]))[-/ ]?((1[0-2])|(0[0-9]))[-/ ]?((20[0-2][0-9])|([0-1][0-9]{3}))", "$1/$4/$7");
	   	}
		return nouvelleDate;
	}
	
	// Note : 0-99 + CT
	public static String valideNote(String note) {
		String nouvelleNote = "";
		// TODO : Vérifier le format
		if (note.matches("(([0-9]{1,2})|CT)")) {
			nouvelleNote = note;
	   	}
		return nouvelleNote;
	}
}
