package mcknightka.tp01;

//pour tester avec vscode
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Tp01Test {
    
    @Test
    public void joursParMoisTest () {
        assertEquals("tester fevrier sur une annee bissextile", 29, Tp01.joursParMois(2, 2000), 0);
        assertEquals("tester janvier, le premier mois dans le tableau de la fonction", 31, Tp01.joursParMois(1, 2000), 0);
        assertEquals("tester decembre, le dernier mois dans le tableau de la fonction", 31, Tp01.joursParMois(12, 2000), 0);
        assertEquals("tester aout, un mois aleatoire", 31, Tp01.joursParMois(8, 2000), 0);
        assertEquals("tester fevrier sur une annee non bissextile", 28, Tp01.joursParMois(2, 2001), 0);
    }

    @Test
    public void reculeDateTest () {
        assertEquals("recule d'une annee et d'un mois", "22 DEC 1999", Tp01.reculeDate(1, 1, 2000, 10));
        assertEquals("recule d'un mois sur une annee bissextile", "26 FEV 2000", Tp01.reculeDate(1, 3, 2000, 4));
        assertEquals("recule d'un mois sur une annee non bissextile", "25 FEV 2001", Tp01.reculeDate(1, 3, 2001, 4));
        assertEquals("recule d'un mois regulier de 31 jours", "26 JUL 2001", Tp01.reculeDate(1, 8, 2001, 6));
        assertEquals("recule a mois que la dixieme journee du mois pour tester le formattage", "05 AVR 2001", Tp01.reculeDate(10, 4, 2001, 5));
    }

    @Test
    public void jourDatePrecedenteTest () {
        assertEquals("recule d'un mois sur une annee non bissextile", 29, Tp01.jourDatePrecedente(1, 3, 2000));
        assertEquals("recule d'un mois sur une annee bissextile", 28, Tp01.jourDatePrecedente(1, 3, 2001));
        assertEquals("teste une valeur aleatoire (aucun saut de mois)", 9, Tp01.jourDatePrecedente(10, 3, 2012));
        assertEquals("recule d'un an", 31, Tp01.jourDatePrecedente(1, 1, 2001));
        assertEquals("recule d'une journee de la fin d'un mois bissextile", 28, Tp01.jourDatePrecedente(29, 2, 2000));
    
    }

    @Test
    public void moisDatePrecedenteTest () {
        assertEquals("change d'annee", 12, Tp01.moisDatePrecedente(1, 1, 2000));
        assertEquals("change a partir de la fin d'un mois", 2, Tp01.moisDatePrecedente(31, 2, 2000));
        assertEquals("change sur un mois bissextile", 2, Tp01.moisDatePrecedente(1, 3, 2000));
        assertEquals("changement typique", 10, Tp01.moisDatePrecedente(9, 10, 2009));
        assertEquals("changement a partir du debut d'un mois", 1, Tp01.moisDatePrecedente(1, 2, 2003));
    
    }

    @Test
    public void anneeDatePrecedenteTest () {
        assertEquals("debut janvier", 1999, Tp01.anneeDatePrecedente(1, 1, 2000));
        assertEquals("etree typique", 2000, Tp01.anneeDatePrecedente(17, 9, 2000));
        assertEquals("premiere journee d'un mois mais pas janvier", 2000, Tp01.anneeDatePrecedente(1, 3, 2000));
        assertEquals("premier mois mais pas journee", 2000, Tp01.anneeDatePrecedente(3, 1, 2000));
        assertEquals("fin d'un mois", 2000, Tp01.anneeDatePrecedente(31, 1, 2000));
    
    }


}
