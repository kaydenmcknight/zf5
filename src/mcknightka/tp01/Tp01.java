package mcknightka.tp01;

import java.util.Scanner;

public class Tp01 {
    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        int jour, mois, annee;
        int decalage;
        System.out.print("Inscrivez une date (JJ MMM AAAA) : ");
        jour = clavier.nextInt();
        mois = moisEnInt(clavier.next());
        annee = clavier.nextInt();
        System.out.print("Inscrivez le décalage : ");
        decalage = clavier.nextInt();
        System.out.print("Nouvelle date : ");
        if (decalage > 0) {
            System.out.print(avanceDate(jour, mois, annee, decalage));
        } else {
            System.out.print(reculeDate(jour, mois, annee, -decalage));
        }
    }

    public static String moisEnStr(int mois) {
        String moisStr;
        switch (mois) {
            case 1:
                moisStr = "JAN";
                break;
            case 2:
                moisStr = "FEV";
                break;
            case 3:
                moisStr = "MAR";
                break;
            case 4:
                moisStr = "AVR";
                break;
            case 5:
                moisStr = "MAI";
                break;
            case 6:
                moisStr = "JUN";
                break;
            case 7:
                moisStr = "JUL";
                break;
            case 8:
                moisStr = "AOU";
                break;
            case 9:
                moisStr = "SEP";
                break;
            case 10:
                moisStr = "OCT";
                break;
            case 11:
                moisStr = "NOV";
                break;
            case 12:
                moisStr = "DEC";
                break;
            default:
                moisStr = "XXX";
                break;
        }
        return moisStr;
    }

    public static int moisEnInt(String moisStr) {
        int mois;
        switch (moisStr) {
            case "JAN":
                mois = 1;
                break;
            case "FEV":
                mois = 2;
                break;
            case "MAR":
                mois = 3;
                break;
            case "AVR":
                mois = 4;
                break;
            case "MAI":
                mois = 5;
                break;
            case "JUN":
                mois = 6;
                break;
            case "JUL":
                mois = 7;
                break;
            case "AOU":
                mois = 8;
                break;
            case "SEP":
                mois = 9;
                break;
            case "OCT":
                mois = 10;
                break;
            case "NOV":
                mois = 11;
                break;
            case "DEC":
                mois = 12;
                break;
            default:
                mois = -1;
                break;
        }
        return mois;
    }

    public static String avanceDate(int jour, int mois, int annee, int decalage) {
        int jourSuivant, moisSuivant, anneeSuivante;
        for (int i = decalage; i > 0; --i) {
            jourSuivant = jourDateSuivante(jour, mois, annee);
            moisSuivant = moisDateSuivante(jour, mois, annee);
            anneeSuivante = anneeDateSuivante(jour, mois, annee);
            jour = jourSuivant;
            mois = moisSuivant;
            annee = anneeSuivante;
        }
        return String.format("%02d %s %d", jour, moisEnStr(mois), annee);
    }

    public static int jourDateSuivante(int jour, int mois, int annee) {
        if (jour == joursParMois(mois, annee)) {
            jour = 1;
        } else {
            jour++;
        }
        return jour;
    }

    public static int moisDateSuivante(int jour, int mois, int annee) {
        if (jour == joursParMois(mois, annee)) {
            mois = (mois % 12) + 1;
        }
        return mois;
    }

    public static int anneeDateSuivante(int jour, int mois, int annee) {
        if (jour == 31 && mois == 12) {
            annee++;
        }
        return annee;
    }

    /**
     * 
     * @param numero du mois
     * @param annee du mois (utilisee pour voir si l'annee est bissextile)
     * @return nb de jours dans le mois
     */
    public static int joursParMois(int mois, int annee) {
        int[] nbJours = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if ((annee % 4 == 0 && annee % 100 != 0) || annee % 400 == 0) {
            nbJours[1] += 1;
        }
        
        return nbJours[mois - 1];
    }

    /**
     * 
     * @param journee en int
     * @param mois en int
     * @param annee, aussi en int
     * @param decalage nombre dejours auquel la date doit etre decalee
     * @return la date reculee, en format string
     */
    public static String reculeDate(int jour, int mois, int annee, int decalage) {
        int rsltJour = jour, rsltMois = mois, rsltAnnee = annee;
        for (int i = 0; i < decalage; i++) {
            rsltJour --;
            if (rsltJour <= 0) {
                rsltMois -= 1;

                if (rsltMois <= 0) {
                    rsltMois = 12;
                    rsltAnnee -= 1;
                }

                rsltJour = joursParMois(rsltMois, rsltAnnee);
            }        


        }

        return String.format("%02d %s %04d", rsltJour, moisEnStr(rsltMois), rsltAnnee);
    }

    /**
     * 
     * @param jour en int
     * @param mois en int
     * @param annee en int
     * @return le numero de la journee precedant la date specifiee
     */
    public static int jourDatePrecedente(int jour, int mois, int annee) {
        String datePrecedente = reculeDate(jour, mois, annee, 1);
        String jourStr = datePrecedente.charAt(0) + "" + datePrecedente.charAt(1);
        return Integer.parseInt(jourStr);
    }

    /**
     * 
     * @param jour en int
     * @param mois en int
     * @param annee en int
     * @return le numero du mois precedant la date specifiee
     */
    public static int moisDatePrecedente(int jour, int mois, int annee) {
        String datePrecedente = reculeDate(jour, mois, annee, 1);
        String moisStr = datePrecedente.charAt(3) + "" + datePrecedente.charAt(4) + "" + datePrecedente.charAt(5);
        return moisEnInt(moisStr);
    }

    /**
     * 
     * @param jour en int
     * @param mois en int 
     * @param annee en int
     * @return l'annee precedente
     */
    public static int anneeDatePrecedente(int jour, int mois, int annee) {
        String datePrecedente = reculeDate(jour, mois, annee, 1);
        String anneeStr = datePrecedente.charAt(7) + "" + datePrecedente.charAt(8) + "" + datePrecedente.charAt(9) + "" + datePrecedente.charAt(10);
        return Integer.parseInt(anneeStr);
    }
}
