package mcknightka.at02;

import static org.junit.Assert.assertEquals;

import org.junit.*;

public class atelier1Test {
    
    @Test
    public void moyenneTest () {
        double[] temp = {30, 40, 20};
        assertEquals(30, atelier1.moyenne(temp), 0);
        double[] temp2 = {60, 50.2, 100};
        assertEquals(70.06666666666, atelier1.moyenne(temp2), 0.01);
    }

    @Test
    public void notePondereeTest () {
        assertEquals(75, atelier1.notePonderee(100, 50, 50, 50), 0);
        assertEquals(13, atelier1.notePonderee(30, 40, 40, 3), 0);
        assertEquals(51, atelier1.notePonderee(60, 50, 70, 30), 0);
    }


    @Test
    public void conversionNoteTest () {
        assertEquals('A', atelier1.conversionNote(90), 0);
        assertEquals('B', atelier1.conversionNote(75), 0);
        assertEquals('C', atelier1.conversionNote(60), 0);
        assertEquals('D', atelier1.conversionNote(50), 0);
        assertEquals('F', atelier1.conversionNote(30), 0);
        assertEquals('X', atelier1.conversionNote(-2), 0);
    }


    @Test
    public void ordreTest () {
        assertEquals("decroissant", atelier1.ordre(3, 2, 1), "decroissant");
        assertEquals("croissant", atelier1.ordre(1, 2, 3), "croissant");
        assertEquals("constant", atelier1.ordre(2, 2, 2), "constant");
        assertEquals("quelconque", atelier1.ordre(2, 2, 3), "quelconque");
    }


    @Test
    public void sommeSuiteTest () {
        assertEquals(15, atelier1.sommeSuite(1, 5, 1), 0);
        assertEquals(25, atelier1.sommeSuite(1, 10, 2), 0);
        assertEquals(15, atelier1.sommeSuite(1, 10, 4), 0);
        assertEquals(25, atelier1.sommeSuite(1, 9, 2), 0);
    }

    @Test
    public void expMaxTest () {
        assertEquals(16, atelier1.expMax(2, 20), 0);
        assertEquals(2401, atelier1.expMax(7, 3000), 0);
        assertEquals(4096, atelier1.expMax(4, 7060), 0);
    }
}
