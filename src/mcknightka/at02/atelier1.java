package mcknightka.at02;

import commun.MenuMaker;

public class atelier1 {
    static MenuMaker menu = new MenuMaker(30);

    public static void main(String[] args) {
        int choix;
        boolean quitter = false;
        String[] options = {
            "Calcul moyenne",
            "Calcul note ponderee",
            "Conversion note",
            "Determine ordre",
            "Calcul somme suite",
            "Exposant maximal",
            "",
            "",
            "Quitter"
        };

        // Declarer les variables
        do{
            menu.cls();
            choix = menu.build("Menu principal", options, true);
            switch (choix) {
                case 1: // Calcul moyenne
                    // Affichage + lire 3 valeurs
                    // Appeler methode moyenne() et recuperer resultat
                    // Afficher resultat
                    double vals[] = new double[3];
                    menu.print("Entrez 3 valeurs separees par des retours a la ligne :");

                    for (int i = 0; i < 3; i++) {
                        vals[i] = Double.parseDouble(menu.prompt("", false));
                    }

                    menu.print("La moyenne vaut " + moyenne(vals));
                    menu.wt();
                    break;
                case 2: // Calcul note ponderee
                    int exam = 0;
                    int pondExam = 0;
                    int travail = 0;
                    int pondTravail = 0;
                    // Affichage + lire 2 valeurs (examens)
                    exam = Integer.parseInt(menu.prompt("Entrez la note de l'examen en valeur entiere"));
                    pondExam = Integer.parseInt(menu.prompt("Entrez la ponderation de l'examen en valeur entiere (%)"));
                    // Affichage + lire 2 valeurs (travaux)
                    travail = Integer.parseInt(menu.prompt("Entrez la note du travail en valeur entiere"));
                    pondTravail = Integer.parseInt(menu.prompt("Entrez la ponderation du travail en valeur entiere (%)"));
                    // Appeler methode notePonderee() et recuperer resultat
                    // Afficher resultat ou message selon valeur de retour
                    menu.print("Le note finale est " + notePonderee(pondExam, exam, pondTravail, travail));
                    menu.wt();
                    break;
                case 3: // Conversion note
                    // Affichage + lire valeur note
                    float note = Integer.parseInt(menu.prompt("Quelle est la note?"));
                    // Appeler methode conversionNote() et recuperer resultat
                    // Afficher resultat
                    menu.print("Le resultat est " + conversionNote(note));
                    menu.wt();
                    break;
                case 4: // Determine ordre
                    // Affichage + lire 3 valeurs
                    int val1 = Integer.parseInt(menu.prompt("entrez la premiere valeur"));
                    int val2 = Integer.parseInt(menu.prompt("entrez la deuxieme valeur"));
                    int val3 = Integer.parseInt(menu.prompt("entrez la troisieme valeur"));
                    // Appeler methode ordre() et recuperer resultat
                    // Afficher resultat
                    menu.print("Les valeurs sont en ordre " + ordre(val1, val2, val3));
                    menu.wt();
                    break;
                case 5: // Calcul somme suite
                    // Affichage + lire valeur depart
                    int valDep = Integer.parseInt(menu.prompt("quelle est la valeur de depart?"));
                    // Affichage + lire valeur finale
                    int valFin = Integer.parseInt(menu.prompt("quelle est la valeur de fin?"));
                    // Affichage + lire valeur pas
                    int valPas = Integer.parseInt(menu.prompt("quelle est la valeur du pas?"));
                    // Appeler methode sommeSuite() et recuperer resultat
                    // Afficher resultat ou message selon valeur de retour
                    menu.print("Resultat: \n" + sommeSuite(valDep, valFin, valPas));
                    menu.wt();
                    break;
                case 6: // Exposant maximal
                    // Affichage + lire valeur base
                    int valBase = Integer.parseInt(menu.prompt("Quelle est la valeur de base"));
                    // Affichage + lire valeur recherchee
                    int valMax = Integer.parseInt(menu.prompt("Quelle est la valeur maximale"));
                    // Appeler methode expMax() et recuperer resultat
                    // Afficher resultat ou message selon valeur de retour
                    menu.print("La valeur la plus proche est " + expMax(valBase, valMax));
                    menu.wt();
                    break;
                case 9: // Quitter
                    quitter = true;
                    break;
                default:
                    menu.print("Choix invalide");
                    break;
            }
        



        menu.line(25);
        choix = 0;
        } while (!quitter); // Ajouter la condition

        menu.cls();
        menu.print("A la prochaine!");
    }

    // Option 1 : Moyenne
    public static double moyenne(double[] vals) {
        double somme = 0;
        for (double d : vals) {
            somme += d;
        }

        return somme / 3;
    }

    // Option 2 : Calcul note ponderee
    public static int notePonderee(int pondEx, int noteEx, int pondTp, int noteTp) {
        return (noteEx * pondEx + noteTp * pondTp)/100;
    }

    // Option 3 : Conversion note
    public static char conversionNote(float notePct) {
        char resultat = 'X';

        char[] lettres = {'A', 'B', 'C', 'D', 'F'};
        float[] notesMinimum = {90, 75, 60, 50, 0};

        for (int i = 0; i < notesMinimum.length; i++) {
            if (notePct >= notesMinimum[i]) {
                resultat = lettres[i];
                break;
            }
        }

        return resultat;
    }

    // Option 4 : Determine ordre
    public static String ordre(int nb1, int nb2, int nb3) {
        String resultat = "";
        if (nb1 > nb2){
            resultat = "decroissant";
        }else if (nb2 > nb1) {
            resultat = "croissant";
        }else {
            resultat = "constant";
        }

        if (resultat == "decroissant" && nb2 > nb3) {
            resultat = "decroissant";
        }else if (resultat == "croissant" && nb3 > nb2) {
            resultat = "croissant";
        }else if (nb2 == nb3){
            resultat = "constant";
        }else {
            resultat = "quelconque";
        }

        return resultat;
    }


    // Option 5 : Calcul somme suite
    public static int sommeSuite(int nbMin, int nbMax, int pas) {
        int resultat = 0;

        if (!(nbMin < nbMax && pas >= 1)) {
            return -1;
        }

        for (int i = nbMin; i <= nbMax; i+= pas) {
            resultat += i;
        }
        return resultat;
    }

    // Option 6 : Exposant maximal
    public static double expMax(int base, int valMax) {
        double resultat = 0;
        for (int i = 0; Math.pow(base, i) <= valMax ;i++) {
            resultat = Math.pow(base, i);
        }
        return resultat;
    }

    


}
