package mcknightka.at03;

import static org.junit.Assert.assertEquals;

import org.junit.Test;//pour tester avec vscode

public class At03Ex01Test {
    
    @Test
    public void sommeTest () {
        assertEquals(3, At03Ex01.somme(1, 2), 0);
        assertEquals(9997, At03Ex01.somme(9999, -2), 0);
        assertEquals(315, At03Ex01.somme(300, 15), 0);
        assertEquals(22, At03Ex01.somme(20, 2), 0);
    }

    @Test
    public void differenceTest () {
        assertEquals(-1, At03Ex01.difference(1, 2), 0);
        assertEquals(10001, At03Ex01.difference(9999, -2), 0);
        assertEquals(285, At03Ex01.difference(300, 15), 0);
        assertEquals(18, At03Ex01.difference(20, 2), 0);
    }

    @Test
    public void multiplicationTest () {
        assertEquals(2, At03Ex01.multiplication(1, 2), 0);
        assertEquals(0, At03Ex01.multiplication(0, 2), 0);
        //assertEquals(Integer.MIN_VALUE, At03Ex01.multiplication(Integer.MAX_VALUE, Integer.MAX_VALUE), 0);//pas capable d'envoyer la valeur maximum a la fonction
        //pas capable de faire une multiplication qui va par dessus le maximum
    }

    @Test
    public void divisionTest () {
        assertEquals(Integer.MIN_VALUE, At03Ex01.division(1, 0), 0);
        assertEquals(3, At03Ex01.division(1500, 500), 0);
        assertEquals(31, At03Ex01.division(1234, 39), 0);
    }

    @Test
    public void moduloTest () {
        assertEquals(Integer.MIN_VALUE, At03Ex01.modulo(1, 0), 0);
        assertEquals(1, At03Ex01.modulo(433, 4), 0);
        assertEquals(1, At03Ex01.modulo(87, 2), 0);
        assertEquals(0, At03Ex01.modulo(56, 1), 0);
    }

    public void pgcdTest () {
        assertEquals(10, At03Ex01.pgcd(10, 20), 0);
        assertEquals(10, At03Ex01.pgcd(10, -20), 0);
        assertEquals(10, At03Ex01.pgcd(300, 10), 0);
        assertEquals(1, At03Ex01.pgcd(1, 200), 0);
    }

    @Test
    public void ppcm () {
        assertEquals(10, At03Ex01.pgcd(10, 20), 0);
        assertEquals(25, At03Ex01.pgcd(100, 125), 0);
        assertEquals(10, At03Ex01.pgcd(300, 10), 0);
        assertEquals(1, At03Ex01.pgcd(1, 200), 0);
    }
}
