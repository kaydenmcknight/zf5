package mcknightka.at03;

import java.util.*;
import java.util.regex.*;

public class At03Ex01 {

    public static void main(String[] args) {
        Scanner clavier = new Scanner(System.in);
        int nb1, nb2, res;
        char op;
        String equation;
        String eqRegEx = "^(-?[0-9]+)[ ]*([+*/%:;-])[ ]*(-?[0-9]+)$";

        do {
            System.out.print("> ");
            equation = clavier.nextLine().trim().toUpperCase();
            res = Integer.MIN_VALUE;

            if (equation.matches(eqRegEx)) {
                Matcher m = Pattern.compile(eqRegEx).matcher(equation);
                m.find();
                nb1 = Integer.parseInt(m.group(1));
                op = m.group(2).charAt(0);
                nb2 = Integer.parseInt(m.group(3));
                switch (op) {
                    // AJOUTER LE TRAITEMENT DES OPERATIONS ICI
                    case '-':
                        res = difference(nb1, nb2);
                        break;
                    case '+':
                        res = somme(nb1, nb2);
                        break;
                    case '*':
                        res = multiplication(nb1, nb2);
                        break;
                    case '/':
                        res = division(nb1, nb2);
                        break;
                    case '%':
                        res = modulo(nb1, nb2);
                    case ':':
                        res = pgcd(nb1, nb2);
                        break;
                }
            } else {
                if (equation.length() > 0) {
                    op = equation.charAt(0);
                } else {
                    op = 'X';
                }
            }
            if (op != 'Q') {
                if (res == Integer.MIN_VALUE) {
                    System.out.println("ERREUR");
                } else {
                    System.out.println(res);
                }
            }
        } while (op != 'Q');
        System.out.println("Au revoir!");
    }

    public static int somme(int nb1, int nb2) {
        return nb1 + nb2;
    }
    
    public static int difference(int nb1, int nb2) {
        return nb1 - nb2;
    }

    public static int multiplication(int nb1, int nb2) {
        long resultat = nb1 * nb2;

        if(resultat > Integer.MAX_VALUE || resultat < Integer.MIN_VALUE){
            return Integer.MIN_VALUE;
        }else{
            return nb1 * nb2;
        }
         
    }

    public static int division(int nb1, int nb2) {
        if (nb2 == 0){
            return Integer.MIN_VALUE;
        }else{
            return nb1 / nb2;
        } 
    }

    public static int modulo(int nb1, int nb2) {
        if (nb2 == 0){
            return Integer.MIN_VALUE;
        }else{
            return nb1 % nb2;
        } 
    }
        
    public static int pgcd(int nb1, int nb2){
        int r = 0;

        while (nb2 != 0) {
            r = nb1 % nb2;
            nb1 = nb2;
            nb2 = r;
        }

        return nb1;
    }

    public static int ppcm(int nb1, int nb2){
        long resultat = pgcd(nb1, nb2) / (nb1 * nb2);
        int r = 0;
        if(resultat < Integer.MIN_VALUE){
            r  = Integer.MIN_VALUE;
        }else {
            r = (int) resultat;
        }


        return r;
    }
}
