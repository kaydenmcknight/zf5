package commun;

import java.io.IOException;
import java.util.*;


/**
 *  Utility for creating java console menus quickly and easily
 * @author Kayden Mc Knight
 * @version 1.0.3
 */
public class MenuMaker {
    private int width = 40;//menu width, determines where and how to center text
    private char borderChar = '-'; //character used for horizontal lines
    private char vertChar = '|'; //character used for vertical lines
    private Scanner input = new Scanner(System.in);
    private String finalText = "";
    private boolean xyFlip = false;//flips x and y in table arguments to how it usually is in game coordinates (x first and y second)
    private String mode = "display";//changes what to do with the text 

    /**
     * Initialiser for MenuMaker
     * @param w width of the menu, default is 40
     * @param bC (optional) character used to make horizontal lines, default is -
     */
    public MenuMaker (int w, char... bC) {
        borderChar = (bC.length >= 1) ? bC[0] : '-';
        width = w;
    }

    public MenuMaker () {}

    /**
     * Builds a menu with options and numbers them if waitForInput (wFI) is true
     * @param title centered title for the menu
     * @param options array of options that you want to have show up
     * @param wFI (optional) waitForInput, adds numbers to the right and returns whatever the user chooses, you can ass empty strings to the options array to skip a number option.
     * @return Whatever the user chose if waitForInput is true, wFI does not work in string mode or buffer mode
     */
    public int build (String title, String[] options, boolean... wFI) {
        boolean waitForInput = (wFI.length >= 1) ? wFI[0] : false;
        if(mode.equals("string") || mode.equals("buffer")){
            waitForInput = false;
        }
        int result = 0;
        boolean done = false;
        

        

        do {
            cls();

            center(title);
            line(width);
            for (int i = 0; i < options.length; i++) {
                if (options[i] != "") {
                    right("[" + (i + 1) + "]", options[i]);
                }
            }
            
            if (waitForInput) {
                //try {
                    result = input.nextInt();
                    done = true;
                //} catch (Exception e){
                //    return outLn("Choix invalide");
                //    done = false;
                //}
            }else {
                done = true;
            }
            
        } while (!done);

        return result;
    }

    /**
     * Creates a data table
     * @param columnWidths width of each column respectively, has to be entire number
     * @param content content of table in [x][y], 0, 0 being the top left and values increasing towards the bottom right
     * @param placeholder default string when no data is set in a cell, default is "***"
     * @return table as a string if display mode is set to string
     */
    public String table (int columnWidths[], String[][] content, String... placeholder) {
        String p = (placeholder.length >= 1) ? placeholder[0]: "***";
        String result = "";
        int spaceCount = 0;
        int columnCount = columnWidths.length;
        int rowCount = content.length;

        for (int i = 0; i < columnWidths.length; i++) {
            spaceCount += columnWidths[i] + 1;
        }

        spaceCount --;

        for(int i = 0; i < rowCount; i++) {//for every row
            result += out(vertChar + "");
            result += line(spaceCount, false);
            result += outLn(vertChar + "");
            result += out(vertChar + "");
            for( int j = 0; j < columnCount; j++){//do every column
                if (xyFlip){
                    result += out(fillToWidth((content[j][i] == null || content[j][i] == "") ? p : content[j][i], columnWidths[j]) + vertChar);
                }else{
                    result += out(fillToWidth((content[i][j] == null || content[i][j] == "") ? p : content[i][j], columnWidths[j]) + vertChar);
                }
            }
            result += outLn("");
        }

        result += out(vertChar + "");
        result += line(spaceCount, false);
        result += outLn(vertChar + "");

        return result;
    }

    private String fillToWidth(String text, int width){
        String result = "";
        for (int i = 0; i < (width - text.length())/2; i++) {
            result += " ";
        }

        result += text;

        for (int i = 0; i < (width - text.length())/2; i++) {
            result += " ";
        }

        if (result.length() < width){ //if width is uneven
            result += " ";
        }

        return result;
    }


    /**
     * Prompts a message and waits for input, clears screen by default 
     * Note: will still wait for input upon execution is display mode is set to string or buffer
     * @param msg Message that will be printed during prompt
     * @param clr (optional) Clear screen before prompt (default is true)
     * @return The user's input as a string
     */
    public String prompt (String msg, boolean... clr) {
        boolean clear = (clr.length >= 1) ? clr[0]: true;
        String result = " ";

        if (clear) {
            cls();
        }

        out(msg);
        result = input.nextLine();

        return result;
    }

    /**
     * Prompts a message and waits for input, clears screen by default
     * Note: will still wait for input upon execution is display mode is set to string or buffer
     * @param msg Message that will be printed during prompt
     * @param clr (optional) Clear screen before prompt (default is true)
     * @return The user's input as an integer
     */
    public int promptInt (String msg,  boolean... clr) {
        return Integer.parseInt(prompt(msg, (clr.length >= 1) ? clr[0]: true));
    }

    /**
     * Prompts a message and waits for input, clears screen by default
     * Note: will still wait for input upon execution is display mode is set to string or buffer
     * @param msg Message that will be printed during prompt
     * @param clr (optional) Clear screen before prompt (default is true)
     * @return The user's input as a double
     */
    public double promptDouble (String msg, boolean... clr) {
        return Double.parseDouble(prompt(msg, (clr.length >= 1) ? clr[0]: true));
    }

    /**
     * Prompts a message and waits for input, clears screen by default
     * Note: will still wait for input upon execution is display mode is set to string or buffer
     * @param msg Message that will be printed during prompt
     * @param clr (optional) Clear screen before prompt (default is true)
     * @return The user's input as a float
     */
    public float promptFloat (String msg, boolean... clr) {
        return Float.parseFloat(prompt(msg, (clr.length >= 1) ? clr[0]: true));
    }

    /**
     * Pauses program until user key input
     * @param msg (optional) message to write before waiting text is still affected by display mode
     */
    public void wt(String... msg){
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //simple print line command, easier than rewriting the whole thing everytime
    /**
     * Prints to console, quicker than writing System.out.print()
     * @param text text to be printed to screen
     * @param clr (optional) clears screen before printing, default is set to true
     * @return text if display mode is set to string
     */
    public String print(String text, boolean... clr) {
        boolean clear = (clr.length >= 1) ? clr[0]: true;

        if (clear) {
            cls();
        }

        return outLn(text);
    }

    /**
     * Clears console display
     */
    public void cls(){
        //return out("\033[H\033[2J");  
        //System.out.flush();
        outLn("\033c");
    }

    private String out(String text) {
        switch (mode) {
            case "string":
                return text;
            case "buffer":
                finalText += text;
                break;
        
            default:
                System.out.print(text);
                break;
        }

        return text;
    }

    private String outLn(String text){
        return out(text + "\n");
    }

    /**
     * Emptys text cache in buffer mode
     */
    public void clrText(){
        finalText = "";
    }

    /**
     * Displays text cache in buffer mode
     * @param clr (optional) Clears text cache after printing, default is false
     */
    public void disp(boolean... clr) {
        boolean clear = (clr.length > 0) ? clr[0] : false;
        System.out.print(finalText);

        if (clear) {
            clrText();
        }
    }

    /**
     * Makes a horizontal line of desired width with current borderChar
     * @param w width of line to make
     * @param ln (optional) returns to next line after drawing if true, default is true
     * @return line if display mode is set to string
     */
    public String line (int w, boolean... ln){
        boolean l = (ln.length > 0) ? ln[0] : true;
        String resultChars = "";
        for (int i = 0; i < w - 1; i++) {
            resultChars += borderChar;
        }
        if (l) {
            return outLn(resultChars + borderChar);
        }else{
            return out(resultChars + borderChar);
        }
        
    }

    //centers inputted text to menu width
    /**
     * Centers text to current menu width
     * @param text text to center
     * @return centered text if display mode is set to string
     */
    public String center (String text) {
        int spaces = (width - text.length())/2;
        String spaceChars = "";

        for (int i = 0; i < spaces; i++) {
            spaceChars += " ";
        }

        return outLn(spaceChars + text);
    }

    /**
     * Alings text to right of current menu width
     * @param text text to center to right of menu
     * @param ttl (optional) textToLeft of the line
     * @return alinged text if display mode is set to string
     */
    public String right (String text, String... ttl) {
        String textToLeft = (ttl.length >= 1) ? ttl[0] : "";
        int spaces = width - (text.length() + textToLeft.length());
        String resultText = textToLeft;

        for (int i = 0; i < spaces; i++) {
            resultText += " ";
        }

        return outLn(resultText + text);
    }

    /**
     * Sets menu width, it is recommended to use an even number for better centering
     * @param w width to set the menu to
     */
    public void setWidth(int w) {
        width = w;
    }

    /**
     * Gets current menu width
     * @return
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets display mode for menus
     * <br> <br>
     * display: <br><br>
     * displays directly to screen
     * <br><br>
     * string:<br><br>
     *  returns a string of the content instead of displaying it
     *  *does not work on functions that can return user choices*
     * <br><br>
     * buffer: <br><br>
     *  puts everything into a buffer that can be saved to a string or displayed at once
     * *to display, use the disp() method, use getText() to retreive content as string, use clrText() to empty content*
     * @param m Mode to set it to, options are : display(default), string, buffer
    */
    public void setMode(String m){
        mode = m;
    }

    /**
     * Get current display mode
     * @return current display mode
     */
    public String getMode(){
        return mode;
    }

    /**
     * Get text cache when display mode is set to buffer
     * @return text cache
     */
    public String getText(){
        return finalText;
    }

    /**
     * Sets horizontal line character for menus
     * @param c character to use for horizontal lines
     */
    public void setBorderChar(char c) {
        borderChar = c;
    }

    /**
     * Sets vertical line character for tables
     * @param c character to use for vertical lines
     */
    public void setVertChar(char c) {
        vertChar = c;
    }

    /**
     * set the xy flip variable
     * @param xyFlip xy flip, switches wheter x is the first or second value in 2d arrays for coordinate functions
     */
    public void setXyFlip(boolean xyFlip){
        this.xyFlip = xyFlip;
    }

    /**
     * get xy flip variable value
     * @return xyflip's value
     */
    public boolean getXyFlip () {
        return xyFlip;
    }


}
